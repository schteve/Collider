## Bonkers
Bonkers is a simulation of 1D collisions between elastic particles.

### Compilation 
    ./Compile.sh

### Usage
    Bonkers ip [op]

### To do
 - [ ] Add scenarios, other plots
    - [ ] Scatter plot of velocities of two particles 
    - [ ] Scatter plot of positions     ''
    - [ ] Histograms
 - [x] Fix plotting speed/printing of lines for different (lower) dt
 - [ ] Do command line options
    - [ ] -h | --help: print usage
    - [x] [ipname] or [ipname] [opname]: do simulation from ip -> op or "myname"
    - [ ] [opname]: infer gnuplot files from op file, do plots
    
 - [x] Add README/TODO
 - [x] Add Licence
 - [x] Add Compile.sh 
 - [x] Tidy up current files
 - [x] Move from printf to iostream 
 - [x] Sort plot optimisation: T/dt/points printed/gnuplot replot rate all hardcoded atm
 - [x] Automatic op based on ipname etc.
    - [x] *ip* -> *op*
    - [x] Add meta data to opname: T, dt
 - [x] Automatic gnuplot on op file and meta data
    - [x] First go via popen/fprintf
    - [x] add automation: number of particles, duration, ...
    - [x] Add T, dt to input file
    - [x] Tidy up: into function(s)
        - [x] Move out of main
        - [x] Divvy up so other plots don't have to redo the same work
 - [x] Add scenarios, other plots 
    - [x] Produce input files for different scenarios
    - [x] Add calculation of KE, P, ...
    - [x] plots of total KE/Momentum
    - [x] plots of total KE
        - [x] in 4ip* plot KE on log-log to show dE/dt
