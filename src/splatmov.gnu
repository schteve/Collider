# Malus.gnu

#PTS="points pt 1 ps 2 lw 5"
#LC="lc rgb"
#PS1="  'light-red'"
#PS2="  'orangered4'"
#PS3="  'olive'"
#PS4="  'green'"
#PS5="  'dark-cyan'"
#PS6="  'royalblue'"
#PS7="  'light-magenta'"
#PS8="  'violet'"

#set terminal epslatex size 3.33, 2 color colortext solid lw 1
#set output "Malus.tex"                                                           

r3 = 640
set term wxt size 640, 640 background rgb 'white'
set border 3 back lc rgb 'black'
set tics nomirror
set ylabel 't'  textcolor rgb 'black'
set xlabel 'x' textcolor rgb 'black'
r = 0.6
r1 = -r
set xrange [r1:r]
set yrange [0:10]
set samples 100000
      
dt = 1
T = 200 
t=0

plot 'op' every 1::t::t u 10:1 noti w points pt 7 ps 1 lw 5 lc rgb 'green',\
     'op' every 1::0::t u 10:1 noti w lines ls 2 lw 3 lc rgb 'green', \
     'op' every 1::t::t u 16:1 noti w points pt 7 ps 2 lw 5 lc rgb 'royalblue',\
     'op' every 1::0::t u 16:1 noti w lines ls 2 lw 3 lc rgb 'royalblue', \
     'op' every 1::0::t u 4:1 noti w lines ls 2 lw 3 lc rgb 'orangered4', \
     'op' every 1::0::t u 22:1 noti w lines ls 2 lw 3 lc rgb 'orangered4', \

while (t < T) {
    if ((t + dt < T)) {
        t = t + dt
        pause 0.025
        replot
    } else {
        pause 5
        t = 0
    }
}
      


#f(x) = 2.0094871555*x-0.0359388519
#g(x) = -2.0948621876*x + 192543046
   
#plot f(x) with lines notitle ls 2 lw 2 lc rgb 'light-red',	     g(x) with lines notitle ls 2 lw 2 lc rgb 'light-red',	     '~/Documents/Docs/Cambridge/II/E1/Report/mintex/MalusData.csv' using 1:2:3 every ::1 notitle with yerr  pt 1 ps 2 lw 5 lc rgb 'light-red', 	     '~/Documents/Docs/Cambridge/II/E1/Report/mintex/MalusDataEx.csv' using 1:2:3 every ::1 notitle with yerr pt 1 ps 2 lw 5 lc rgb 'royalblue'
   
     
      #'MalusData.csv' using 1:2:3 every ::1 notitle with points pt 1 ps 2 lw 5 lc rgb 'light-red', 	
