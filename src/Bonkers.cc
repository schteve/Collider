/* Bonkers.cc 
 * 
 * Part of Bonkers, a simulation of 1D Collisions between elastic particles
 * 
 * Copyright © 2017-2018 Steven Moseley <stevenmos@kolabnow.com>
 * Bonkers is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "Particle.h"
#include "BonFunc.h"
#include <vector>
#include <string>
#include <limits>
#include <cstdio>

int main(int argc, char** argv)
{
    std::vector<Particle> Marbles; // Define vector of Classical particles
    const double RefT = 5, Refdt = 0.025;// Define ref, total, inc times
    // During read, print initial state, but do not increment t 
    double T = 10, dt = 0.025, t =  dt;  
    int lines = RefT / dt; 
    std::string ipname, opname = "";

    Input(Marbles, T, dt, ipname, opname, argc, argv);
    if (opname.empty()) {
        Setopname(ipname, opname, T, dt, Marbles);
    }

    unsigned globcount = 1, TRatio = (Refdt * T) / (RefT * dt);

    std::ofstream fout(opname, std::ofstream::app);
    if (fout.is_open()) {
        while (t < T) {
            double closeTime = std::numeric_limits<double>::max();
            unsigned index = 0;
           
            // which pair next collide
            TimeToNext(Marbles, closeTime, index);

            // Print until collision
            PrintToColl(fout, Marbles, closeTime, t, T, dt, globcount, TRatio);

            // Collide pair
            Collide(Marbles[index], Marbles[index-1]);
        }
    } else {
        std::cerr << opname << " did not open" << std::endl;
    }
    fout.close();

    Plot(1, opname, Marbles, T, dt, lines);
    Plot(2, opname, Marbles, T, dt, lines);
    Plot(0, opname, Marbles, T, dt, lines);

    return 0;
}
