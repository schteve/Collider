/* BonFunc.cc Non-inline function definitions for Planets.cc */

#include "BonFunc.h"

void Input(std::vector<Particle>& Marbles, double &T, double &dt, 
        std::string &ipname, std::string &opname, int argc, char **&argv)
{
    if (argc > 1) {
        ipname = argv[1];
        if (argc > 2) {
            opname = argv[2];
        }         
    } else {
        std::cerr << "No input filename" << std::endl;
    }

    std::ifstream fin(ipname);
    std::string dummy = "";
    fin >> T >> dt;
    ReadAll(Marbles, fin); // Read in from input file
    fin.close();

    for (Particle &P : Marbles) {
        P.CalcPKE();
    }
}

void Setopname(std::string &ipname, std::string &opname, double &T, 
            const double &dt, std::vector<Particle> &Marbles) 
{
    opname = ipname;
    unsigned index = opname.find_first_of("ip");
    unsigned Tp = T;

    if (index != std::string::npos) {
        opname.replace(index, 2, "op");
    }
    opname = opname + "_" + std::to_string(Tp) + "_" + std::to_string(dt);

    std::ofstream fout(opname); // Clear previous run
    if (fout.is_open()) {
        PrintState(fout, Marbles, 0);//PrintAll(Marbles); // Print state 
    } else {
        std::cerr << opname << " did not open" << std::endl;
    }
    fout.close();
}

void ReadAll(std::vector<Particle> &Marbles, std::istream& is) 
{
    int cnt = 0;
    Particle dummy;

    while (read(is, dummy)) {
        Marbles.push_back(dummy);

        if (dummy.im() < pow(10, -10)) { 
            ++cnt;
            if (cnt == 2) {
                break;
            }
        }
    }
}

void PrintAll(std::vector<Particle> &Marbles)
{
    printf("%12s %12s %12s %12s %12s %12s\n", "im", "rad", "x", "v","P","KE");

    for (const auto &P : Marbles) {
        P.PrintParticle();
        printf("\n");
    }
}

void PrintState(std::ostream &os, std::vector<Particle> &Marbles, 
        const double &t)
{
    SetStrSci(os);
    os << t << "  ";

    for (const auto &P : Marbles) {
        P.PrintParticle(os);
    }

    os << "\n";
}

void PrintState(std::vector<Particle> &Marbles, const double &t)
{
    printf("%12.6g", t);

    for (const auto &P : Marbles) {
        P.PrintParticle();
    }

    printf("\n");
}

void TimeToNext(std::vector<Particle> &Marbles, double &closeTime, 
                    unsigned &index)
{
    double dummy = 0.0; 

    for (unsigned u = 1; u < Marbles.size(); ++u) {
        double dspeed = CSpeed(Marbles[u], Marbles[u-1]);// dummyspeed 
        if (dspeed < 0) {
            dummy = CTime(Marbles[u], Marbles[u-1], dspeed);
            
            if (dummy < closeTime) {
                closeTime = dummy;
                index = u;
            }
        }
    }
}

void PrintToColl(std::ofstream &os, std::vector<Particle> &Marbles, 
        double &closeTime, double &t, double &T, const double &dt,
        unsigned &globcount, unsigned &TRatio)
{
    if (TRatio < 1) {
        TRatio = 1;
    }

    for (double dummyt = 0.0; dummyt < closeTime && t < T; 
            dummyt += dt, t += dt) {
        for (unsigned u = 0; u < Marbles.size(); ++u) {
            Marbles[u].x()[0] += Marbles[u].v()[0] * dt;
        }

        if (globcount == TRatio) {
            PrintState(os, Marbles, t);
            globcount = 1;
        } else {
            ++globcount;
        }
    }
}

void Plot(const unsigned &which, const std::string &opname, 
        const std::vector<Particle> &Marbles, const double &T, const double &dt,
        const unsigned &lines)
{
    FILE *pipe = popen("gnuplot", "w");
    if (pipe != NULL) {
        PlotSetup(pipe);

        switch(which) {
            case 0: // Movie
                MovieSetup(pipe, T, lines); 
                fprintf(pipe, (PlotString(which, opname, Marbles)).c_str());
                MoviePostSetup(pipe, dt);
                break;
            case 1: // P
                PSetup(pipe);
                fprintf(pipe, (PlotString(which, opname, Marbles)).c_str());
                PPostSetup(pipe);
                break;
            case 2: // KE
                KESetup(pipe, opname);
                fprintf(pipe, (PlotString(which, opname, Marbles)).c_str());
                KEPostSetup(pipe);
                break;
            default:
                std::cerr << "Not a valid plot type" << std::endl;
                return;
        }
        fflush(pipe);

    } else {
        std::cerr << "Could not open pipe" << std::endl;
    }
    pclose(pipe);

}

void PlotSetup(FILE *pipe)
{
    fprintf(pipe, "set border 3 back lc rgb 'black'\n");
    fprintf(pipe, "set tics nomirror\n");
}

void MovieSetup(FILE *pipe, const double &T, const unsigned &lines)
{
    std::string sone = "", stwo = std::to_string(0), sthr = "";
    std::string result = sone + stwo + sthr;

    fprintf(pipe, "set term wxt size 640, 640 background rgb 'white'\n");
    fprintf(pipe, "set ylabel 't' textcolor rgb 'black'\n");
    fprintf(pipe, "set xlabel 'x' textcolor rgb 'black;\n");
    fprintf(pipe, "r = 0.55\n");
    fprintf(pipe, "r1 = -r\n");
    fprintf(pipe, "set xrange [r1:r]\n");
    sone = "set yrange [0:", stwo = std::to_string(T), sthr = "]\n";
    result = sone + stwo + sthr;
    fprintf(pipe, result.c_str());
    fprintf(pipe, "set samples 1000\n");
    fprintf(pipe, "\n");
    fprintf(pipe, "dt = 1\n");
    sone = "T = "; stwo = std::to_string(lines); sthr = "\n";
    result = sone + stwo + sthr;
    fprintf(pipe, result.c_str());
    fprintf(pipe, "t = 0\n");
    fprintf(pipe, "\n");
}

void MoviePostSetup(FILE *pipe, const double &dt)
{
    std::string sone = "", stwo = std::to_string(0), sthr = "";
    std::string result = sone + stwo + sthr;

    fprintf(pipe, "\n");
    fprintf(pipe, "while (t < T) {\n");
    fprintf(pipe, "    if ((t + dt < T)) {\n");
    fprintf(pipe, "        t = t + dt\n");
    sone = "       pause "; stwo = std::to_string(0.025); sthr = "\n";
    result = sone + stwo + sthr;
    fprintf(pipe, result.c_str());
    fprintf(pipe, "        replot\n");
    fprintf(pipe, "    }    else {\n");
    fprintf(pipe, "         pause 5\n");
    fprintf(pipe, "         t = 0\n");
    fprintf(pipe, "    }\n");
    fprintf(pipe, "}\n");
}

void PSetup(FILE *pipe)
{
    fprintf(pipe, "set term wxt persist size 640, 640\
            background rgb 'white'\n");
    fprintf(pipe, "set ylabel 't/s' textcolor rgb 'black'\n");
    fprintf(pipe, "set xlabel 'P/Ns' textcolor rgb 'black;\n");
    fprintf(pipe, "set xrange [*:*]\n");
    fprintf(pipe, "set yrange [0:*]\n");
}

void PPostSetup(FILE *pipe)
{
    fprintf(pipe, "\n");
    fprintf(pipe, "        replot\n");
}

void KESetup(FILE *pipe, const std::string &opname)
{
    fprintf(pipe, "set term wxt persist size 640, 640\
            background rgb 'white'\n");
    fprintf(pipe, "set ylabel 't/s' textcolor rgb 'black'\n");
    fprintf(pipe, "set xlabel 'KE/J' textcolor rgb 'black;\n");
    fprintf(pipe, "set xrange [*:*]\n");
    fprintf(pipe, "set yrange [*:*]\n");
    if (opname[0] == '4') {
        fprintf(pipe, "set ylabel 'log(t/s)' \n");
        fprintf(pipe, "set xlabel 'log(KE/J)' \n");
    }
}

void KEPostSetup(FILE *pipe)
{
    fprintf(pipe, "\n");
    fprintf(pipe, "        replot\n");
}

std::string PlotString(const unsigned &which, const std::string &opname, 
                        const std::vector<Particle> &Marbles)
{
    std::string plotstring = "plot "; // Marbles.size()  4 + 6n

    std::string ball1 = "' every 1::t::t u ";
    std::string ball2 = ":1 noti w points pt 7 ps 2 lw 5 lc rgb '";
    std::string ball3 = "', '";
    std::string ball4 = "' every 1::0::t u ";
    std::string ball5 = ":1 noti w lines ls 2 lw 3 lc rgb '";
    std::string ball6 = "',";
    std::string colour = "green";

    if (which != 0) {
        ball1 = "' u ";
        ball4 = "' u ";
    }
   
    if (!(opname[0] == '4' && which == 2)) { 
    for (unsigned u = 0; u < Marbles.size(); ++u) {
        unsigned n = Index(which, u);
        std::string ball = "'" + opname;

        if ((u == 0) || (u == (Marbles.size() - 1))) {
            ball += ball4 + std::to_string(n) + ball5 + colour + ball6;

            ColourReplace(ball, colour, "orangered4");
            if (which == 1 || which == 2) {
                ball = "";
            }
        } else {
            if (which == 0) {
                ball += ball1 + std::to_string(n) + ball2 + colour + ball3 + 
                    opname + ball4 + std::to_string(n) + ball5 + colour + ball6;
            } else {
                ball += ball4 + std::to_string(n) + ball5 + colour + ball6;
            }

            if (u%2) {
                ColourReplace(ball, colour, "royalblue");
            }

            if (opname[0] == '5') {
                if (u == 6) {
                    ColourReplace(ball, colour, "light-red");
                } else if (which == 1 || which == 2) {
                    ball = "";
                }
            }
        }
        
        plotstring += ball;
    }
    }

    if (which == 1 || which == 2) {
        std::string ball = "'" + opname + ball4;
        std::string sum = "(";
        unsigned UpperLimit = Marbles.size();

        if (opname[0] == '4' && which == 2) {
            UpperLimit -= 6;
        }

        for (unsigned u = 0; u < UpperLimit; ++u) {
            sum += "$" + std::to_string(Index(which, u)) + " + ";
        }
        sum.replace(sum.rfind(" +"), 2, ")");
        ball += sum + ball5 + colour + ball6;

        ColourReplace(ball, colour, "light-red");
        if (opname[0] == '4' && opname[3] == '1' && which == 2) {
            ball = "'" + opname + "' u (log" + sum + "):(log($1)) noti w lines ls 2 lw 3 lc rgb 'light-red', 0.2 * x + 1.85 ls 2 lw 3 lc rgb 'royalblue'";
        }
        if (opname[0] == '4' && opname[3] == '2' && which == 2) {
            ball = "'" + opname + "' u (log" + sum + "):(log($1)) noti w lines ls 2 lw 3 lc rgb 'light-red', -2 * x - 2.8  ls 2 lw 3 lc rgb 'royalblue'";
        }
        plotstring += ball;
    }

    return plotstring; 
}

unsigned Index(const unsigned &which, const unsigned &u)
{
    unsigned ind = 6 * u;

    switch(which) {
        case 0:
            return 4 + ind;
        case 1:
            return 6 + ind;
        case 2:
            return 7 + ind;
        default:
            std::cerr << "Plot requested does not exist" << std::endl;
            return 4 + ind; 
    }
}
void ColourReplace(std::string &s, const std::string &old, 
                    const std::string &rep)
{
        std::string::size_type pos;
    do {
        pos = s.find(old);
        if (pos < s.size()) {
            s.replace(pos, old.size(), rep);
        }
    } while (pos != std::string::npos && pos < s.size());
}
