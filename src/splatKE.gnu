# Malus.gnu

#PTS="points pt 1 ps 2 lw 5"
#LC="lc rgb"
#PS1="  'light-red'"
#PS2="  'orangered4'"
#PS3="  'olive'"
#PS4="  'green'"
#PS5="  'dark-cyan'"
#PS6="  'royalblue'"
#PS7="  'light-magenta'"
#PS8="  'violet'"

#set terminal epslatex size 3.33, 2 color colortext solid lw 1
#set output "Malus.tex"                                                           

set border 3 back lc rgb 'black'
set tics nomirror
set term wxt persist size 960,640 background rgb 'white'
set ylabel 't'  textcolor rgb 'black'                                                                            
set xlabel 'x' textcolor rgb 'black'                                                                            
set xrange [0:*]
set yrange [0:*]
      

plot '2op1_30_0.025000' using 7:1 notitle with lines ls 3 lw 5 lc rgb 'orangered4', '2op1_30_0.025000' using 25:1 notitle with lines ls 3 lw 5 lc rgb 'orangered4', '2op1_30_0.025000' using 13:1 notitle with lines lw 5 lc rgb 'green', '2op1_30_0.025000' using 19:1 notitle with lines lw 5 lc rgb 'royalblue', '2op1_30_0.025000' using ($7 + $13 + $19 + $25):1 notitle with lines ls 3 lw 5 lc rgb 'light-red' 


#f(x) = 2.0094871555*x-0.0359388519
#g(x) = -2.0948621876*x + 192543046
   
#plot f(x) with lines notitle ls 2 lw 2 lc rgb 'light-red',	     g(x) with lines notitle ls 2 lw 2 lc rgb 'light-red',	     '~/Documents/Docs/Cambridge/II/E1/Report/mintex/MalusData.csv' using 1:2:3 every ::1 notitle with yerr  pt 1 ps 2 lw 5 lc rgb 'light-red', 	     '~/Documents/Docs/Cambridge/II/E1/Report/mintex/MalusDataEx.csv' using 1:2:3 every ::1 notitle with yerr pt 1 ps 2 lw 5 lc rgb 'royalblue'
   
     
      #'MalusData.csv' using 1:2:3 every ::1 notitle with points pt 1 ps 2 lw 5 lc rgb 'light-red', 	
