/* Particle.h Definition of Paticle class, representing classical particle */

#ifndef PARTICLE_H
#define PARTICLE_H

#include <iostream>
#include <algorithm>
#include <numeric>
#include <iterator>
#include <cstdio>

class Particle;
std::istream &read(std::istream&, Particle&);
void SetStrSci(std::ostream &os);

class Particle {
public:
    // Constructors
    Particle() = default;
    Particle(std::istream &is)
    { read(is, *this); }
    static constexpr const int dim = 1;
private:
    // im, rad, x[dim], v[dim], P[dim], KE[dim]
    double params[2] = {};// 0 im, 1 rad
    double xarr[dim] = {};
    double varr[dim] = {};
    double Parr[dim] = {}; 
    double KEval = 0;
public:
    double &im(); 
    const double &im() const;
    double &rad();
    const double &rad() const;
    
    double (&x())[dim];
    const double (&x() const)[dim] ;
    double (&v())[dim];
    const double (&v() const)[dim] ;
    double (&P())[dim];
    const double (&P() const)[dim] ;

    double &KE();
    const double &KE() const;

    void CalcPKE();
    void PrintParticle() const;
    void PrintParticle(std::ostream &os) const;
};

#endif
