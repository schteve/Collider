/* BonFunc.h Headers for BonFunc.cc */

#include "Particle.h"
#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>

// Declarations
void Input(std::vector<Particle>& Marbles, double &T, double &dt, 
        std::string &ipname, std::string &opname, int argc, char **&argv);
void Setopname(std::string &ipname, std::string &opname, double &T, 
            const double &dt, std::vector<Particle> &Marbles);
void ReadAll(std::vector<Particle> &Marbles, std::istream& is);
void PrintAll(std::vector<Particle> &Marbles);
void PrintState(std::ostream &os, std::vector<Particle> &Marbles, 
        const double &t);
void PrintState(std::vector<Particle> &Marbles, const double &t);
void TimeToNext(std::vector<Particle> &Marbles, double &closeTime, 
                    unsigned &index);
void PrintToColl(std::ofstream &os, std::vector<Particle> &Marbles, 
        double &closeTime, double &t, double &T, const double &dt,
        unsigned &globcount, unsigned &TRatio);
void Plot(const unsigned &which, const std::string &opname, 
        const std::vector<Particle> &Marbles, const double &T, const double &dt,
        const unsigned &lines);
void PlotSetup(FILE *pipe);
void MovieSetup(FILE *pipe, const double &T, const unsigned &lines);
void MoviePostSetup(FILE *pipe, const double &dt);
void PSetup(FILE *pipe);
void PPostSetup(FILE *pipe);
void KESetup(FILE *pipe, const std::string &opname);
void KEPostSetup(FILE *pipe);
std::string PlotString(const unsigned &which, const std::string &opname, 
                        const std::vector<Particle> &Marbles);
unsigned Index(const unsigned &which, const unsigned &u);
void ColourReplace(std::string &s, const std::string &old, 
                    const std::string &rep);
inline void Collide(Particle &one, Particle &two);
inline double NewV(const double &im1, const double &v1, const double &im2, 
                    const double &v2);
inline double CSpeed(Particle &one, Particle &two);
inline double CTime(Particle &one, Particle &two, const double CSpeed);

// Inline Definitions
inline void Collide(Particle &one, Particle &two)
{
    double a = 0, b = 0;
    a = NewV(one.im(), one.v()[0], two.im(), two.v()[0]);
    b = NewV(two.im(), two.v()[0], one.im(), one.v()[0]);
    one.v()[0] = a;  
    two.v()[0] = b; 
    one.CalcPKE();
    two.CalcPKE();
}

inline double NewV(const double &im1, const double &v1, const double &im2, 
                    const double &v2)
{
    double m1 = pow(im1, -1);
    double m2 = pow(im2, -1);

    return ((m1 * v1 + m2 * (2 * v2 - v1)) / (m1 + m2));
}

inline double CSpeed(Particle &one, Particle &two) 
{
    return one.v()[0] - two.v()[0];
}

inline double CTime(Particle &one, Particle &two, const double CSpeed)
{
    return (one.x()[0] - two.x()[0] - one.rad() - two.rad()) / 
                            fabs(CSpeed);
}
