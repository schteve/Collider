/* Particle.cc Externally defined functions for Particle.h */

#include "Particle.h"

double& Particle::im()            // Move messy defns outside class
{ return params[0]; }
const double& Particle::im() const
{ return params[0]; }
double& Particle::rad()
{ return params[1]; }
const double& Particle::rad() const
{ return params[1]; }

double (& Particle::x())[dim]
{ return xarr; }
const double (& Particle::x() const)[dim] 
{ return xarr; }
double (& Particle::v())[dim]
{ return varr; }
const double (& Particle::v() const)[dim] 
{ return varr; } 
double (& Particle::P())[dim]
{ return Parr; }
const double (& Particle::P() const)[dim] 
{ return Parr; } 

double& Particle::KE()
{ return params[0]; }
const double& Particle::KE() const
{ return params[0]; }

void Particle::CalcPKE()
{
    std::transform(std::begin(varr), std::end(varr), std::begin(Parr),
            [this] (const double &d) { return d * 1 / params[0]; });
    KEval = 0.5 * (1/params[0]) * 
                std::accumulate(std::begin(varr), std::end(varr), 0.0,
            // Apparently (x,y) x corresponds to running total, y to 
            // dereferenced iterator, don't know why
            [] (double result, const double &d) { return result += d * d; });
}

void Particle::PrintParticle() const
{
    for (const auto &d : params) {
        printf("%12.6g ", d);
    }
    for (const auto &d : xarr) {
        printf("%12.6g ", d);
    }
    for (const auto &d : varr) {
        printf("%12.6g ", d);
    }
    for (const auto &d : Parr) {
        printf("%12.6g ", d);
    }
    printf("%12.6g ", KEval);
}

void Particle::PrintParticle(std::ostream &os) const
{
    SetStrSci(os);

    for (const auto &d : params) {
        os << d << "  ";
    }
    for (const auto &d : xarr) {
        os << d << "  ";
    }
    for (const auto &d : varr) {
        os << d << "  ";
    }
    for (const auto &d : Parr) {
        os << d << "  ";
    }
    os << KEval << "  ";
}

void SetStrSci(std::ostream &os)
{
    os.precision(8);
    os.width(14); // 1. [.8.] e+??
    os << std::scientific;
}

std::istream &read(std::istream &is, Particle &one)
{
    is >> one.im() >> one.rad() >> one.x()[0] >> one.v()[0];
    return is;
}
