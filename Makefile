# MAKEFILE for S6
# 
# Modified from the original by David MacKay 
# http://www.inference.org.uk/mackay/

# if you want the debugger kdbg to work nicely, REMOVE the -O2 flag
# if you want to get all warnings enabled, INCLUDE the -O2 flag

CFLAGS = $(INCDIRS)  \
	-pedantic -g -O2\
	-Wall -Wconversion\
	-Wformat  -Wshadow\
	-Wpointer-arith -Wcast-qual -Wwrite-strings\
	-D__USE_FIXED_PROTOTYPES__

LIBS = -l stdc++ -lm

CXX = g++

objects = bin/Bonkers.o bin/BonFunc.o bin/Particle.o

Bonkers: $(objects)
	$(CXX) $(CFLAGS) $(LIBS) -o bin/Bonkers $(objects)

bin/Bonkers.o: src/Bonkers.cc src/BonFunc.h src/Particle.h
	$(CXX) $(CFLAGS) $(LIBS) -c -o bin/Bonkers.o src/Bonkers.cc

bin/BonFunc.o: src/BonFunc.cc src/BonFunc.h
	$(CXX) $(CFLAGS) $(LIBS) -c -o bin/BonFunc.o src/BonFunc.cc

bin/Particle.o: src/Particle.cc src/Particle.h
	$(CXX) $(CFLAGS) $(LIBS) -c -o bin/Particle.o src/Particle.cc

.PHONY: clean
clean:
	rm bin/*.o

.PHONY: cleanop
cleanop:
	rm *op*
